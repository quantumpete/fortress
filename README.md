# Fortress
A simple [ASCII](http://www.ascii-codes.com/) [Roguelike](https://en.wikipedia.org/wiki/Roguelike) using [OpenGL](https://www.opengl.org/), [GLUT](https://www.opengl.org/resources/libraries/glut/) and [SOIL](www.lonesock.net/soil.htm).

The game is currently in Alpha. For more information, see the [Wiki](https://github.com/pkuehne/fortress/wiki)
