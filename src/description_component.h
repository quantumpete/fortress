#ifndef DESCRIPTION_COMPONENT_H
#define DESCRIPTION_COMPONENT_H

struct DescriptionComponent {
    std::string title;
    std::string text;
};

#endif
