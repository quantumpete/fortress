#include "combat_system.h"
#include "health_component.h"
#include <iostream>

void CombatSystem::handleEvent (const Event* event)
{
    switch (event->getType()) {
        case EVENT_ATTACK_ENTITY: {
            const AttackEntityEvent* l_event = dynamic_cast<const AttackEntityEvent*> (event);

            std::vector<EntityId> l_targets = m_engine->getEntities()->findEntitiesToThe (l_event->direction, l_event->entity);
            std::vector<EntityId>::iterator iter = l_targets.begin();
            for (; iter != l_targets.end(); iter++) {
                HealthComponent* l_health = m_engine->getEntities()->getHealths()->get(*iter);
                if (l_health) {
                    l_health->health--;
                    if (l_health->health < 1) {
                        m_engine->getEntities()->destroyEntity (*iter);
                    }
                    break;
                }
            }
        } break;
        default:break;
    }
}
